﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Windows.Forms;
using ExportFile;

namespace Maticsoft.Model
{
    public class Columns
    {
        public static ArrayList ary_col = new ArrayList();
        public static DataTable dt = new DataTable();
        public static String strName { get; set; }

        /// <summary>
        /// 设置列名
        /// </summary>
        /// <param name="dgvDb"></param>
        public static void SetColumns(DataGridView dgvDb)
        {
            try
            {
                ary_col.Clear();
                for (int i = 0; i < dgvDb.Columns.Count; i++)
                {
                    ary_col.Add(dgvDb.Columns[i].Name.Trim());
                }
            }
            catch { }
        }

        /// <summary>
        /// 设置列名
        /// </summary>
        /// <param name="obj"></param>
        public static void SetColumns(ListBox obj)
        {
            try
            {
                ary_col.Clear();
                for (int i = 0; i < obj.Items.Count; i++)
                {
                    ary_col.Add(obj.Items[i].ToString().Trim());
                }
            }
            catch { }
        }

        /// <summary>
        /// 设置列名
        /// </summary>
        /// <param name="obj"></param>
        public static void SetColumns(DataTable obj)
        {
            try
            {
                ary_col.Clear();
                foreach (DataColumn s in obj.Columns)
                {
                    ary_col.Add(s.ColumnName.Trim());
                }
            }
            catch { }
        }
    }
}

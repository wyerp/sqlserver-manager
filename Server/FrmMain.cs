﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using System.IO;
using Maticsoft.BLL;
using Maticsoft.Model;
using System.Threading;
using Maticsoft.Common;
using System.Collections;

namespace Server
{
    public partial class FrmMain : Office2007Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        //新建表名
        public static String tableName = "";
        #region 方法
        /// <summary>
        /// 绑定服务器数据
        /// </summary>
        public void cboServerBing()
        {
            try
            {
                cboServerList.DataSource = Maticsoft.Common.Servers.GetServers();
                cboServerList.DisplayMember = "ServerName";
                cboServerList.ValueMember = "ServerName";
                cboServerList.SelectedIndex = 0;
            }
            catch { }
        }

        /// <summary>
        /// 绑定数据表数据
        /// </summary>
        /// <param name="bol"></param>
        public void cboDatabaseBing(Boolean bol)
        {
            try
            {
                if (!cboServerList.Text.Equals(""))
                {
                    DataTable dt = Maticsoft.Common.Servers.GetDatabasess(cboServerList.Text.Trim(), txtUid.Text.Trim(), txtPwd.Text.Trim(), bol);
                    if (dt == null)
                    {
                        MessageBox.Show("服务器错误", "提示");
                        cboServerList.SelectedIndex = 0;
                        cboServerList.Focus();
                    }
                    else
                    {
                        cboDatabase.DataSource = dt;
                        cboDatabase.DisplayMember = "name";
                        cboDatabase.ValueMember = "name";
                        cboDatabase.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("请选服务器", "提示");
                    cboServerList.SelectedIndex = 0;
                    cboServerList.Focus();
                }
            }
            catch { }
        }

        /// <summary>
        /// 绑定数据表数据
        /// </summary>
        public void cboTableBing()
        {
            try
            {
                if (!cboDatabase.Text.Equals(""))
                {
                    DataTable dt = Maticsoft.Common.Servers.GetTables(cboDatabase.Text.Trim());
                    if (dt == null)
                    {
                        MessageBox.Show("数据库错误", "提示");
                        cboDatabase.SelectedIndex = 0;
                        cboDatabase.Focus();
                    }
                    else
                    {
                        cboTable.DataSource = dt;
                        cboTable.DisplayMember = "name";
                        cboTable.ValueMember = "name";
                        cboTable.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("请选数据库", "提示");
                    cboDatabase.SelectedIndex = 0;
                    cboDatabase.Focus();
                }
            }
            catch { }
        }

        /// <summary>
        /// 显示文件夹
        /// </summary>
        /// <returns></returns>
        public DialogResult ShowDirectory()
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel File(*.xls)|*.xls";
                sfd.FileName = "数据报表.xls";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    txtFile.Text = sfd.FileName;
                    return DialogResult.OK;
                }
            }
            catch { }
            return DialogResult.Cancel;
        }

        /// <summary>
        /// 绑定查询结果
        /// </summary>
        /// <param name="dt"></param>
        public void DgvSelectBing(Object dt)
        {
            try
            {
                r.Text = "";
                c.Text = "";
                if (dt.ToString().Equals("ok"))
                {
                    dgvSelect.DataSource = null;
                    tabControl3.SelectedTab = tabItem5;
                    rtxt.Clear();
                    rtxt.AppendText("--------执行成功--------\n");
                }
                else
                {
                    tabControl3.SelectedTab = tabItem4;
                    dgvSelect.DataSource = dt;
                }
                r.Text = dgvSelect.Rows.Count.ToString() + "  行";
                c.Text = dgvSelect.Columns.Count.ToString() + "  列";
            }
            catch
            { }
        }

        /// <summary>
        /// 绑定预览数据
        /// </summary>
        public void GetDgv()
        {
            try
            {
                dgvDb.DataSource = frmMain.GetYLDB(cboTable.Text.Trim());
            }
            catch { }
        }

        /// <summary>
        /// 导出数据
        /// </summary>
        public void Export()
        {
            try
            {
                frmMain.Export(txtFile.Text.Trim(), cboTable.Text.Trim());
            }
            catch { }
        }

        /// <summary>
        /// 导出数据
        /// </summary>
        public void ExportAll()
        {
            try
            {
                frmMain.Export(txtFile.Text.Trim(), dgvSelect);
            }
            catch { }
        }

        /// <summary>
        /// 匹对模板
        /// </summary>
        /// <param name="ch_dt"></param>
        /// <returns></returns>
        public Boolean CheckColumn(DataTable ch_dt)
        {
            try
            {
                DataTable dtd = frmMain.GetYLDB(cboTable.Text.Trim());
                if (ch_dt.Columns.Count != dtd.Columns.Count - 1) return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 其他按钮不可用
        /// </summary>
        public void btn_Enabled(Boolean bo)
        {
            if (bo)
            {
                btnImport.Enabled = btnNewTable.Enabled = buttonItem1.Enabled = bo;
                return;
            }
            btnImport.Enabled = !btnImport.Enabled;
            btnNewTable.Enabled = !btnNewTable.Enabled;
            buttonItem1.Enabled = !buttonItem1.Enabled;
        }

        /// <summary>
        /// 显示列标识
        /// </summary>
        public void Check_Column()
        {
            try
            {
                Servers.new_db.Clear();
                Servers.old_db.Clear();
                //添加标识列
                ColumnDY a = new ColumnDY();
                ArrayList b = new ArrayList();
                DataTable dt1 = frmMain.GetYLDB(cboTable.Text.Trim());
                ArrayList bb = new ArrayList();
                foreach (DataColumn c in ((DataTable)(dgvImport.DataSource)).Columns)
                {
                    b.Add(c.ColumnName);
                }
                foreach (DataColumn f in dt1.Columns)
                {
                    bb.Add(f.ColumnName);
                }
                a.ar = b;
                a.ar1 = bb;
                a.ShowDialog();
            }
            catch { }
        }

        /// <summary>
        /// 导入数据
        /// </summary>
        public void import()
        {
            try
            {
                frmMain.SetImport(dgvImport, cboTable.Text.Trim(), jin);
            }
            catch { }
        }
        #endregion

        #region 事件
        //初始化事件
        private void FrmMain_Load(object sender, EventArgs e)
        {
            //cboServerBing();
            cboServerList.SelectedIndex = 0;
            tabControl1_Click(null, null);
            btn_Enabled(false);
        }

        //重新搜索服务器
        private void btnServer_Click(object sender, EventArgs e)
        {
            cboServerBing();
        }

        //登录身份
        private void rboW_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbo = sender as RadioButton;
            if (rbo.Tag.ToString().Equals("a") && rbo.Checked)
            {
                groupPanel.Enabled = true;
            }
            else groupPanel.Enabled = false;
        }

        //刷新数据表
        private void btnRes_Click(object sender, EventArgs e)
        {
            cboTableBing();
        }

        //刷新数据库
        private void btnDatabase_Click(object sender, EventArgs e)
        {
            if (groupPanel.Enabled)
            {
                if (!txtUid.Text.Equals("") && !txtPwd.Text.Equals(""))
                {
                    cboDatabaseBing(true);
                }
                else MessageBox.Show("用户名，密码不能为空", "提示");
            }
            else cboDatabaseBing(false);
        }

        //折叠事件
        private void expandableSplitter1_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            try
            {
                splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
            }
            catch
            { }
        }

        //折叠panel2
        private void expandableSplitter2_ExpandedChanged(object sender, ExpandedChangeEventArgs e)
        {
            try
            {
                splitContainer2.Panel2Collapsed = !splitContainer2.Panel2Collapsed;
            }
            catch
            { }
        }

        //窗体尺寸改变事件
        private void FrmMain_Resize(object sender, EventArgs e)
        {
            try
            {

            }
            catch
            { }
        }

        //自定义导出报表
        private void btnZDExport_Click(object sender, EventArgs e)
        {
            try
            {
                splitContainer1.Panel1Collapsed = !splitContainer1.Panel1Collapsed;
            }
            catch
            { }
        }

        // 浏览文件夹
        private void btn_Click(object sender, EventArgs e)
        {
            try
            {
                ShowDirectory();
                //数据预览
                GetDgv();
            }
            catch { }
        }

        //导出报表
        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                Columns.ary_col.Clear();
                if (cboTable.FindStringExact(cboTable.Text.Trim()) < 0)
                {
                    MessageBox.Show("请连接数据库", "提示");
                    return;
                }
                if (txtFile.Text.Trim().Equals("")) ShowDirectory();
                //设置列
                if (dgvDb.Columns.Count > 0)
                {
                    Columns.SetColumns(dgvDb);
                    if (MessageBox.Show("是否进行列的选择Y/N", "提示", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
                    {
                        new SetColumns().ShowDialog();
                        if (Columns.ary_col.Count <= 0) return;
                    }
                }
                //导出数据
                Export();
            }
            catch { }
        }

        //执行查询
        private void btnZx_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboDatabase.FindStringExact(cboDatabase.Text.Trim()) >= 0)
                {
                    if (cboTable.FindStringExact(cboTable.Text.Trim()) < 0)
                    {
                        MessageBox.Show("请连接数据库", "提示");
                        return;
                    }
                    Object dtb = new Object();
                    if (rtxtSelect.SelectedText.Trim().Equals(""))
                        dtb = frmMain.GetSelectDB(rtxtSelect.Text.Trim().ToLower());
                    else dtb = frmMain.GetSelectDB(rtxtSelect.SelectedText.Trim().ToLower());
                    rtxt.Clear();
                    DgvSelectBing(dtb);
                }
                else
                {
                    MessageBox.Show("请你选择数据库", "提示");
                }
            }
            catch { }
        }

        //导出数据
        private void btnExportAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvSelect.Rows.Count > 0)
                {
                    Columns.ary_col.Clear();
                    if (txtFile.Text.Trim().Equals("")) ShowDirectory();
                    Columns.SetColumns(dgvSelect);
                    //导出数据
                    ExportAll();
                }
                else
                {
                    MessageBox.Show("没有数据无法导出", "提示");
                }
            }
            catch { }
        }

        //错误处理事件
        private void dgvDb_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            try
            {
                rtxt.AppendText("第" + e.RowIndex.ToString() + "行" + "第" + e.ColumnIndex + "列--------类型错误--------\n");
            }
            catch { }
        }

        // 导出tab单击事件
        private void tabControl1_Click(object sender, EventArgs e)
        {
            r.Visible = true;
            c.Visible = true;
            jin.Visible = false;
            d.Visible = false;
        }

        // 导入tab单击事件
        private void tabItem2_Click(object sender, EventArgs e)
        {
            r.Visible = false;
            c.Visible = false;
            jin.Visible = true;
            d.Visible = true;
        }

        //打开文件
        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboServerList.FindStringExact(cboServerList.Text.Trim()) >= 0)
                {
                    dgvImport.DataSource = null;
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "数据报表|*.xls";
                    ofd.Multiselect = false;
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        DataTable dt = frmMain.GetXls(ofd.FileName);
                        if (dt != null)
                        {
                            dgvImport.DataSource = dt;
                            d.Text = string.Format("共 {0} 行", dt.Rows.Count);
                            btn_Enabled(true);
                        }
                        else
                        {
                            MessageBox.Show("没有数据", "提示");
                        }
                    }
                }
                else
                    MessageBox.Show("选择服务器", "提示");
            }
            catch
            {
                btn_Enabled(true);
            }
        }

        //添加对应列
        private void buttonItem1_Click(object sender, EventArgs e)
        {
            Check_Column();
        }

        //执行导入
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboTable.FindStringExact(cboTable.Text.Trim()) >= 0)
                {
                    DataTable dt_import = (DataTable)dgvImport.DataSource;
                    if (Servers.new_db.Count <= 0)
                        if (MessageBox.Show("没有选择标识列,这样数据只会插入\n是否选择标识列", "提示", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
                        {
                            //选择标识列
                            Check_Column();
                        }
                        else
                        {
                            if (!CheckColumn(dt_import))
                            {
                                if (MessageBox.Show("模板不匹配是否重新创建模板\n并导入数据", "提示", MessageBoxButtons.YesNo).Equals(DialogResult.Yes))
                                {
                                    //创建数据表
                                    CreateTable crt = new CreateTable();
                                    crt.dt = ((DataTable)(dgvImport.DataSource));
                                    crt.ShowDialog();
                                    Servers.new_db.Clear();
                                    Servers.old_db.Clear();
                                    return;
                                }
                                else return;
                            }
                        }
                    //正常导入
                    import();
                    Servers.new_db.Clear();
                    Servers.old_db.Clear();
                    d.Text = "";
                }
                else
                {
                    MessageBox.Show("请选择数据表", "提示");
                }
            }
            catch { }
        }

        //导入到新表
        private void btnNewTable_Click(object sender, EventArgs e)
        {
            //创建数据表
            CreateTable crt = new CreateTable();
            crt.dt = ((DataTable)(dgvImport.DataSource));
            crt.bol = true;
            crt.ShowDialog();
            Servers.new_db.Clear();
            Servers.old_db.Clear();
            try
            {
                frmMain.SetImport(dgvImport, tableName, jin);
            }
            catch { }
        }

        DataTable dtd = new DataTable();
        //测试数据事件
        private void tabItem2_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (cboTable.FindStringExact(cboTable.Text.Trim()) < 0)
                {
                    MessageBox.Show("请连接数据库", "提示");
                    tabControl2.SelectedTab = tabSelect;
                    return;
                }
                cboCS.Items.Clear();
                dtd = frmMain.GetYLDB(cboTable.Text.Trim());
                dtd.Rows.Clear();
                dgvCS.DataSource = dtd;
                cboCS.Items.Add("无标识列");
                for (int ii = 0; ii < dtd.Columns.Count; ii++)
                {
                    cboCS.Items.Add(dtd.Columns[ii].ColumnName.ToString().Trim());
                }
                cboCS.SelectedIndex = 0;
            }
            catch { }
        }

        //执行测试数据
        private void buttonItem2_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(t.ControlText.Trim()) > 0)
                {
                    if (cboTable.FindStringExact(cboTable.Text.Trim()) >= 0)
                    {
                        dgvCS.DataSource = frmMain.SetCS(dtd, int.Parse(t.ControlText.Trim()), cboTable.Text.Trim(), cboCS.ControlText.Trim());
                    }
                }
                else
                {
                    MessageBox.Show("数据行输入错误", "提示");
                }
            }
            catch
            {
                MessageBox.Show("数据行输入错误", "提示");
            }
        }

        //窗体关闭事件
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifyIcon1.Icon = null;
            System.Diagnostics.Process[] myProces = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process proc in myProces)
                if (proc.ProcessName == "Server")
                    proc.Kill();
        }

        //窗体关闭
        private void 关闭ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
